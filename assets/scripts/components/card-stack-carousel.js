'use strict';

(function () {
    initialize();

    function initialize() {
        $('.card-stack--container').each(function (index) {
            $(this).attr('id', 'card-stack--container-' + index);
        });

        $('.card-pagination li').click(function () {
            const parentId = $(this).parents('.card-stack--container').attr('id'),
                clickedIndex = $(this).index(),
                activeIndex = $('#' + parentId + ' .card-pagination li.active').index();

            let delay = 0;
            if (clickedIndex < activeIndex) {
                for (let i = 0; i < Math.abs(clickedIndex - activeIndex); i++) {
                    setTimeout(function () {
                        prev(parentId);
                    }, delay);
                    delay += 400;
                }
            } else {
                for (let i = 0; i < Math.abs(clickedIndex - activeIndex); i++) {
                    setTimeout(function () {
                        next(parentId);
                    }, delay);
                    delay += 400;
                }
            }
        });

        $('.quote .next').click(function () {
            next($(this).parents('.card-stack--container').attr('id'));
        });

        $('.quote .prev').click(function () {
            prev($(this).parents('.card-stack--container').attr('id'));
        });
    }

    function next(parentId) {
        const quotes = $('#' + parentId + ' .card-stack .card-list .quote'),
            firstQuote = quotes[0],
            lastQuote = quotes.slice(-1)[0];

        $(firstQuote).addClass('next-card');

        setTimeout(function () {
            $(firstQuote).insertAfter($(lastQuote));
            setActive(parentId, 'next');
            $(firstQuote).removeClass('next-card').removeClass('active');

            setTimeout(function () {
                $('#' + parentId + ' .card-stack .card-list .quote:first-child').addClass('active');
            }, 100);
        }, 300);
    }

    function prev(parentId) {
        const quotes = $('#' + parentId + ' .card-stack .card-list .quote'),
            firstQuote = quotes[0],
            lastQuote = quotes.slice(-1)[0];

        $(lastQuote).addClass('active prev-card');
        $(lastQuote).insertBefore($(firstQuote));

        setTimeout(function () {
            setActive(parentId, 'prev');
            $(lastQuote).removeClass('prev-card');

            setTimeout(function () {
                $('#' + parentId + ' .card-stack .card-list .quote.active:not(:first-child)').removeClass('active');
            }, 100);
        }, 350);
    }

    function setActive(parentId, direction) {
        const activePagination = $('#' + parentId + ' .card-pagination li.active'),
            nextPagination = direction === 'prev' ? activePagination.prev() : activePagination.next();

        $(activePagination).removeClass('active');

        if (nextPagination.length > 0) {
            $(nextPagination).addClass('active');
        } else {
            const child = direction === 'prev' ? 'last' : 'first';
            $('#' + parentId + ' .card-pagination li:' + child + '-child').addClass('active');
        }
    }

})();
